#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 14:25:17 2023

@author: asmae 
"""

import argparse

def main(args):
    #Receiving the input word list    
    listwords = input("\033[1;4m"+"Enter the wanted list of words seperated with spaces:"+"\033[0m"+"\n").split()
    # Sort the list according to the option given in argument
    #the sort order ascending
    if args.sort == "1":
        listwords.sort()
    #the sort order descending
    elif args.sort == "0":
        listwords.sort(reverse=True)
    # Delete double words if the option is specified
    if args.unique:
        listwords = list(set(listwords))
    # Display
    if args.output:
        with open(args.output, "w") as f:
            f.write("\033[1;4m"+"he final list is:"+"\033[0m"+"\n".join(listwords))
    else:
        print("\033[1;4m"+"The final list is:"+"\033[0m"+"\n".join(listwords))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sort", choices=["1", "0"], default="",
                        help="Specify the sort order (ascending (1) or descending(0))")
    parser.add_argument("--unique", action="store_true",
                        help="Delete the duplicate words")
    parser.add_argument("--output", 
                        help="Save the sorted")
    args = parser.parse_args()
    main(args)

#by applying the following command we can get a new list in order descending and deleting the duplicate words
#python3 appliAE.py --sort 0 --unique
